from fastapi import FastAPI
from .models.operations import ItemModel
from .storage.list import coffees


app = FastAPI()


## 2. Если этот роут ничего полезного не делает,  то как забрать и вывести весь список кофе? А если по id?
@app.get('/')
def root():
    return {"key": "Hello World!"}



## 1. Данный роут ничего не делает, а должен создавать новый элемент, которого еще небыло. Идея метода - Insert New Item.
@app.post('/add')
async def send_data(data: ItemModel): ## 2. Наверное, название метода должно быть add_data, а лучше - add_coffee
    return data
###########################################################################################################################


@app.put('/update')
async def update_data(data: ItemModel):
    coffees[data.id]["name"] = data.name
    coffees[data.id]["price"] = data.price
    return coffees[data.id]


@app.delete('/delete/{id}')
async def delete_data(id: int):
    del coffees[id]
    return coffees
